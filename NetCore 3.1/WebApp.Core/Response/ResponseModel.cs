﻿namespace WebApp.Core.Response
{
    public class ResponseModel
    {
        public int Status { get; set; }
        public dynamic Result { get; set; }
    }
}
