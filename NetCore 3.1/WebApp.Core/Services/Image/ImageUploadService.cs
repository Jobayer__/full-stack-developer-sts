﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Threading.Tasks;
using WebApp.Core.Interfaces.Image;

namespace WebApp.Core.Services.Image
{
    public class ImageUploadService : IImageUploadService
    {
        /// <summary>
        /// Get form data (image) from client and upload to wwwroot/Images directory and return image location
        /// </summary>
        /// <param name="Files"></param>
        /// <param name="environment"></param>
        /// <returns></returns>
        dynamic IImageUploadService.UploadImage(IFormFileCollection Files, IHostEnvironment environment)
        {
            IFormFile file = Files[0];
            try
            {
                string newPath = Path.Combine(environment.ContentRootPath, "wwwroot/Images");
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }

                if (file.Length > 0)
                {
                    string imageName = DateTime.Now.ToString("ddMMyyhhmmss") + ".png"; ;
                    string fullPath0 = Path.Combine(newPath, imageName);

                    using (var stream = new FileStream(fullPath0, FileMode.Create))
                    {
                        Task result = Task.Run(() => file.CopyToAsync(stream));
                        result.Wait();
                    }

                    string imageUrl = "images/" + imageName;
                    return imageUrl;
                }
                return string.Empty;
            }
            catch (Exception) { return string.Empty; }
        }
    }
}
