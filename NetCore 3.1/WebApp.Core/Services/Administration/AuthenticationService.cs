﻿using Microsoft.Extensions.Configuration;
using WebApp.Core.Interfaces.Account;
using WebApp.Core.Services.Repository;
using WebApp.Domain.Entities;
using WebApp.Domain.Models;
using WebApp.Infrastructure.Context;

namespace WebApp.Core.Services.Administration
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly ApplicationDbContext context;
        private readonly Repository<User> repository;
        public AuthenticationService(ApplicationDbContext _context)
        {
            this.context = _context;
            this.repository = new Repository<User>(_context);
        }

        /// <summary>
        /// From controller, the following method will call to register a User
        /// </summary>
        /// <param name="user"></param>
        /// <returns>After completing registration it will retuen, newly registered User data</returns>
        public dynamic UserRegistrationAsync(User user)
        {
            UserRegistration registration = new UserRegistration(this.context);
            return registration.Registration(user, this.repository);
        }

        /// <summary>
        /// From controller the following method will call to check authentication of an User 
        /// </summary>
        /// <param name="userLoginModel"></param>
        /// <param name="configuration"></param>
        /// <returns>Will return JWT token</returns>
        public dynamic UserAuthenticationAsync(UserLoginModel userLoginModel, IConfiguration configuration)
        {
            AuthenticateUser authenticateUser = new AuthenticateUser(this.context);
            return authenticateUser.Authenticate(userLoginModel, configuration);
        }
    }
}
