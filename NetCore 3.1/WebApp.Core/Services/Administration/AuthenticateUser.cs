﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Security.Claims;
using System.Text;
using WebApp.Domain.Models;
using WebApp.Infrastructure.Context;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Domain.Entities;
using System.IdentityModel.Tokens.Jwt;
using WebApp.Core.Services.ExtensionMethod;

namespace WebApp.Core.Services.Administration
{
    class AuthenticateUser
    {
        private readonly ApplicationDbContext context;
        public AuthenticateUser(ApplicationDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Check User data exist or not by email and password
        /// If exist then create JWT and return
        /// Else return false
        /// </summary>
        /// <param name="userLoginModel"></param>
        /// <param name="configuration"></param>
        /// <returns>JWT Token</returns>
        public dynamic Authenticate(UserLoginModel userLoginModel, IConfiguration configuration)
        {
            userLoginModel.Password = userLoginModel.Password.MD5Encryption();

            Task<User> result = Task.Run(() => this.GetuserByCredential(userLoginModel));
            result.Wait();

            User userModel = result.Result;
            if (userModel == null) return new AuthResponseModel() { Status = false, Token = null };

            return this.CreateJWT(userModel, configuration);
        }

        /// <summary>
        /// Create and return JWT Token for an valid User
        /// </summary>
        /// <param name="userModel"></param>
        /// <param name="configuration"></param>
        /// <returns>JWT Token</returns>
        private AuthResponseModel CreateJWT(User userModel, IConfiguration configuration)
        {
            SymmetricSecurityKey securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:SecretKey"]));
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, userModel.FullName),
                new Claim(ClaimTypes.Email, userModel.Email),
                new Claim(ClaimTypes.NameIdentifier, userModel.Id.ToString()),
                new Claim("IssueTime", DateTime.Now.ToString())
            };

            SecurityTokenDescriptor securityTokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = DateTime.Now.AddDays(30),
                Issuer = configuration["Jwt:Issuer"],
                Audience = configuration["Jwt:Audience"],
                Subject = new ClaimsIdentity(claims),
                SigningCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature)
            };

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            JwtSecurityToken token = tokenHandler.CreateJwtSecurityToken(securityTokenDescriptor);

            AuthResponseModel authResponse = new AuthResponseModel(userModel)
            {
                Token = tokenHandler.WriteToken(token)
            };

            return authResponse;
        }

        /// <summary>
        /// Check user exist or not by email and password
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        private User GetuserByCredential(UserLoginModel loginModel)
        {
            User user = context.Users.Where(user => user.Email == loginModel.Email && user.Password == loginModel.Password).FirstOrDefault();
            return user;
        }
    }
}
