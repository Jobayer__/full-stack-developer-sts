﻿using System.Linq;
using System.Threading.Tasks;
using WebApp.Core.Response;
using WebApp.Core.Services.ExtensionMethod;
using WebApp.Core.Services.Repository;
using WebApp.Domain.Entities;
using WebApp.Infrastructure.Context;

namespace WebApp.Core.Services.Administration
{
    public class UserRegistration
    {
        private readonly ApplicationDbContext context;
        public UserRegistration(ApplicationDbContext _context)
        {
            this.context = _context;
        }

        /// <summary>
        /// First check User already exist ot not
        /// If exist, return existing data
        /// Else add User to Database
        /// </summary>
        /// <param name="user"></param>
        /// <param name="repository"></param>
        /// <returns>User data</returns>
        public dynamic Registration(User user, Repository<User> repository)
        {
            Task<User> alreadyExist = Task.Run(() => this.GetuserByEmail(user.Email));
            alreadyExist.Wait();

            if (alreadyExist.Result != null) return new ResponseModel { Result = alreadyExist.Result, Status = 409 };

            user.Password = user.Password.MD5Encryption();
            Task<User> createNew = Task.Run(() => repository.AddAsync(user));
            createNew.Wait();

            return new ResponseModel { Result = createNew.Result, Status = 201 };
        }

        /// <summary>
        /// To check email already exist or not
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private User GetuserByEmail(string email)
        {
            User user = context.Users.Where(user => user.Email == email).FirstOrDefault();
            return user;
        }
    }
}
