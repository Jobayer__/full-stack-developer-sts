﻿using System.Security.Cryptography;
using System.Text;

namespace WebApp.Core.Services.ExtensionMethod
{
    public static class EncryptPassword
    {
        /// <summary>
        /// Generate an MD5 encrypted string
        /// </summary>
        /// <param name="password"></param>
        /// <returns>encrypted string</returns>
        public static string MD5Encryption(this string password)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] encrypt;
            UTF8Encoding encode = new UTF8Encoding();
            encrypt = md5.ComputeHash(encode.GetBytes(password));
            StringBuilder encryptdata = new StringBuilder();
            
            for (int i = 0; i < encrypt.Length; i++)
            {
                encryptdata.Append(encrypt[i].ToString());
            }

            return encryptdata.ToString();
        }
    }
}
