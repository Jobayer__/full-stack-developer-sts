﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApp.Domain.Models;
using WebApp.Infrastructure.Context;

namespace WebApp.Core.Services.Meeting
{
    class EventList
    {
        private readonly ApplicationDbContext context;
        public EventList(ApplicationDbContext _applicationDbContext)
        {
            this.context = _applicationDbContext;
        }

        /// <summary>
        /// Call Store procedure [GetEventListByUsers] to get event list by User id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="skip"></param>
        /// <param name="count"></param>
        /// <returns>A list of event list to that corrosponding User</returns>
        public IEnumerable<EventModel> GetEventList(long id, int skip, int count)
        {
            Task<IEnumerable<EventModel>> result = Task.Run(() => this.Get(id, skip, count));
            result.Wait();

            return result.Result;
        }

        /// <summary>
        /// Store Procedure Call to get event list
        /// </summary>
        /// <param name="id"></param>
        /// <param name="skip"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        private async Task<IEnumerable<EventModel>> Get(long id, int skip, int count)
        {
            var _id = new SqlParameter("@Id", id);
            var _skip = new SqlParameter("@Skip", skip);
            var _count = new SqlParameter("@Count", count);

            IEnumerable<EventModel> result = await context.Set<EventModel>().FromSqlRaw("[dbo].[GetEventListByUsers] {0}, {1}, {2}", _id, _skip, _count).ToListAsync();
            return result;
        }
    }
}



