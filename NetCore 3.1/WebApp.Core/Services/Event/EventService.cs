﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WebApp.Core.Interfaces.Event;
using WebApp.Core.Response;
using WebApp.Core.Services.Repository;
using WebApp.Domain.Models;
using WebApp.Infrastructure.Context;

namespace WebApp.Core.Services.Meeting
{
    public class EventService : IEventService
    {
        private readonly ApplicationDbContext context;
        private readonly Repository<Domain.Entities.Event> repository;
        public EventService(ApplicationDbContext _applicationDbContext)
        {
            this.context = _applicationDbContext;
            this.repository = new Repository<Domain.Entities.Event>(_applicationDbContext);
        }

        /// <summary>
        /// From controller the following method will call to add an Event to Database
        /// </summary>
        /// <param name="meeting"></param>
        /// <returns>Newly created event data</returns>
        public dynamic AddEventAsync(Domain.Entities.Event meeting)
        {
            Task<Domain.Entities.Event> createNew = Task.Run(() => repository.AddAsync(meeting));
            createNew.Wait();

            return new ResponseModel { Result = createNew.Result, Status = 201 };
        }

        /// <summary>
        /// From controller the following method will call to delete an Event by event id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public dynamic DeleteEventByIdAysnc(int id)
        {
            Task createNew = Task.Run(() => repository.DeleteAsync(id));
            createNew.Wait();

            return new ResponseModel { Result = id, Status = 200 };
        }

        /// <summary>
        /// From controller the following method will call to edit and Event
        /// </summary>
        /// <param name="meeting"></param>
        /// <returns>Edited event data</returns>
        public dynamic EditEventAsync(Domain.Entities.Event meeting)
        {
            Task<Domain.Entities.Event> createNew = Task.Run(() => repository.EditAsync(meeting));
            createNew.Wait();

            return new ResponseModel { Result = createNew.Result, Status = 201 };
        }

        /// <summary>
        /// From controller the following method will call to an event by event id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Single event model</returns>
        public dynamic GetEventByIdAysnc(int id)
        {
            Task<Domain.Entities.Event> createNew = Task.Run(() => repository.GetByIdAsync(id));
            createNew.Wait();

            return new ResponseModel { Result = createNew.Result, Status = 201 };
        }

        /// <summary>
        /// From controller the following method will call to get event list by User id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="skip"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public dynamic GetEventListAysnc(int id, int skip, int count)
        {
            EventList eventList = new EventList(this.context);
            Task<IEnumerable<EventModel>> result = Task.Run(() => eventList.GetEventList(id, skip, count));
            result.Wait();

            return new ResponseModel { Result = result.Result, Status = 200 };
        }
    }
}
