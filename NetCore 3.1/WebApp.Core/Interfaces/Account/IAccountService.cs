﻿using Microsoft.Extensions.Configuration;
using WebApp.Domain.Entities;
using WebApp.Domain.Models;

namespace WebApp.Core.Interfaces.Account
{
    public interface IAuthenticationService
    {
        public dynamic UserRegistrationAsync(User user);
        public dynamic UserAuthenticationAsync(UserLoginModel userLoginModel, IConfiguration configuration);
    }
}
