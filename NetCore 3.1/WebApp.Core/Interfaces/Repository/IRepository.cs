﻿using System.Threading.Tasks;

namespace WebApp.Core.Interfaces.Repository
{
    public interface IRepository<T> where T : class
    {
        Task<T> AddAsync(T entity);
        Task<T> EditAsync(T entity);
        Task<T> GetByIdAsync(int id);
        Task DeleteAsync(int id);
    }
}
