﻿namespace WebApp.Core.Interfaces.Event
{
    public interface IEventService
    {
        public dynamic AddEventAsync(Domain.Entities.Event meeting);
        public dynamic EditEventAsync(Domain.Entities.Event meeting);
        public dynamic GetEventListAysnc(int id, int skip, int count);
        public dynamic GetEventByIdAysnc(int id);
        public dynamic DeleteEventByIdAysnc(int id);
    }
}
