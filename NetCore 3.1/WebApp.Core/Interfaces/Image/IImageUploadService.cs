﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;

namespace WebApp.Core.Interfaces.Image
{
    public interface IImageUploadService
    {
        dynamic UploadImage(IFormFileCollection Files, IHostEnvironment environment);
    }
}
