﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Domain.Auth
{
    public class AccessToken
    {
        [Required]
        public string Token { get; set; }
        public int? StatusCode { get; set; }
        public DateTime ExpireTime { get; set; }
    }
}
