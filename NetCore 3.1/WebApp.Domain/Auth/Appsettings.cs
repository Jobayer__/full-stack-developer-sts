﻿namespace WebApp.Domain.Auth
{
    public class Appsettings
    {
        public string SecurityKey { get; set; }
        public string ValidIssuer { get; set; }
        public string ValidAudience { get; set; }
    }
}
