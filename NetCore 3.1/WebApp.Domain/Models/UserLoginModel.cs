﻿namespace WebApp.Domain.Models
{
    public class UserLoginModel
    {
        // Getter Setter
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
