﻿using System;
using WebApp.Domain.Entities;

namespace WebApp.Domain.Models
{
    public class AuthResponseModel : UserModel
    {
        public bool Status { get; set; }
        public string Token { get; set; }
        public DateTime ExpireTime { get; set; }

        public AuthResponseModel() { }

        public AuthResponseModel(User userModel)
        {
            Id = userModel.Id;
            FullName = userModel.FullName;
            Email = userModel.Email;
            Image = userModel.Image;
            BirthDate = userModel.BirthDate;
            Status = true;
        }
    }
}
