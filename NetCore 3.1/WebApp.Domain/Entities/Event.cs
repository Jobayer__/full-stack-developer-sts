﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Domain.Entities
{
    public class Event
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTimeOffset Date { get; set; }
        public TimeSpan TimeFrom { get; set; }
        public TimeSpan TimeTo { get; set; }
        public string Location { get; set; }
        public int NotifyTime { get; set; }
        public string Label { get; set; }
        public DateTimeOffset ModifiedTime { get; set; }
        public bool IsDeleted { get; set; }

        [ForeignKey("UserId")]
        public User Users { get; set; }
    }
}
