﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Domain.Entities
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Image { get; set; }
        public DateTimeOffset BirthDate { get; set; }
        public bool IsActive { get; set; }
    }
}
