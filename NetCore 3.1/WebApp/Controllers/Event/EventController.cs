﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApp.Core.Interfaces.Event;

namespace WebApp.Controllers.Meeting
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventController : ControllerBase
    {
        private readonly IEventService eventService;
        public EventController(IEventService _meetingService)
        {
            this.eventService = _meetingService;
        }

        [HttpPost("Add")]
        public dynamic AddMeeting([FromBody] Domain.Entities.Event meeting)
        {
            return this.eventService.AddEventAsync(meeting);
        }

        [HttpPut("Edit")]
        public dynamic EditMeeting([FromBody] Domain.Entities.Event meeting)
        {
            return this.eventService.EditEventAsync(meeting);
        }

        [HttpGet("GetList/{id}/{skip}/{count}")]
        public dynamic GetMeetingList([FromRoute] int id, [FromRoute] int skip, [FromRoute] int count)
        {
            return this.eventService.GetEventListAysnc(id, skip, count);
        }

        [HttpGet("Get/{id}")]
        public dynamic GetMeetingById([FromRoute] int id)
        {
            return this.eventService.GetEventByIdAysnc(id);
        }

        [HttpDelete("{id}")]
        public dynamic DeleteById([FromRoute] int id)
        {
            return this.eventService.DeleteEventByIdAysnc(id);
        }
    }
}
