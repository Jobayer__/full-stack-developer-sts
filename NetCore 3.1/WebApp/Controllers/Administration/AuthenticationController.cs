﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebApp.Core.Interfaces.Account;
using WebApp.Domain.Entities;
using WebApp.Domain.Models;

namespace WebApp.Controllers.Account
{
    [Route("api/[controller]")]
    [ApiController]

    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationService accountService;
        private readonly IConfiguration configuration;
        public AuthenticationController(IAuthenticationService _accountService, IConfiguration _configuration)
        {
            this.accountService = _accountService;
            this.configuration = _configuration;
        }

        [HttpPost("Signup")]
        public dynamic UserRegistrationAsync([FromBody] User user)
        {
            return accountService.UserRegistrationAsync(user);
        }

        [HttpPost("Login")]
        public dynamic UserAuthenticationAsync([FromBody] UserLoginModel userLoginModel)
        {
            return accountService.UserAuthenticationAsync(userLoginModel, this.configuration);
        }
    }
}
