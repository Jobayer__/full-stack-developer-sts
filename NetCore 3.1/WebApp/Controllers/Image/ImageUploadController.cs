﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Hosting;
using WebApp.Core.Interfaces.Image;
using System.Threading.Tasks;

namespace WebApp.Controllers.Image
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageUploadController : ControllerBase
    {
        private readonly IImageUploadService imageUploadService;
        private readonly IHostEnvironment environment;

        public ImageUploadController(IHostEnvironment _environment, IImageUploadService imageUploadService)
        {
            this.imageUploadService = imageUploadService;
            this.environment = _environment ?? throw new ArgumentNullException(nameof(environment));
        }

        [HttpPost]
        public IActionResult PostImage()
        {
            IFormFileCollection files = Request.Form.Files;
            Task<dynamic> result = Task.Run(() => this.imageUploadService.UploadImage(files, this.environment));
            result.Wait();

            return Ok(result.Result.ToString());
        }
    }

}
