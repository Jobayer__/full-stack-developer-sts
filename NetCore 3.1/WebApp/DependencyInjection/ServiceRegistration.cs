﻿using System;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Core.Services.Image;
using System.Collections.Generic;
using WebApp.Core.Interfaces.Event;
using WebApp.Core.Services.Meeting;
using WebApp.Core.Interfaces.Image;
using WebApp.Core.Interfaces.Account;
using WebApp.Core.Services.Administration;
using Microsoft.Extensions.DependencyInjection;

namespace WebApp.DependencyInjection
{
    public class ServiceRegistration
    {
        public void Register(IServiceCollection services)
        {
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IEventService, EventService>();
            services.AddScoped<IImageUploadService, ImageUploadService>();
        }
    }
}
