import { Component, OnInit, Inject } from '@angular/core';
import { EventService } from '../service/event.service';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-modal-dialog',
  templateUrl: './modal-dialog.component.html',
  styleUrls: ['./modal-dialog.component.scss']
})

export class ModalDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private eventService: EventService) { }

  ngOnInit() {
    console.log('inject: ', this.data)
  }

  public async onDeleteClick() {
    await this.eventService.deleteEvent(this.data.id);
  }
}