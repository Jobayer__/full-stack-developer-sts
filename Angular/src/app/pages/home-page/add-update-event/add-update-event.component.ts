import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/core/services/storage/storage.service';
import { StorageKey } from 'src/app/core/services/storage/storage.model';
import { EventService } from '../service/event.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { EventModel } from 'src/app/model/event-model';
const { USER_DATA } = StorageKey;
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-update-event',
  templateUrl: './add-update-event.component.html',
  styleUrls: ['./add-update-event.component.scss']
})

export class AddUpdateEventComponent implements OnInit {

  user: any;
  eventModel: EventModel;

  // Dummy notify item list
  notifyTimeList = [{ id: 10, value: '10 minutes' }, { id: 20, value: '20 minutes' }, { id: 30, value: '30 minutes' }];

  // Get User data from local storage, because userId need while adding or editing data.
  constructor(private eventService: EventService, private storage: StorageService, private router: ActivatedRoute, private snackBar: MatSnackBar) {
    this.user = JSON.parse(this.storage.read(USER_DATA));
  }

  /**
   * Check event its a edit or add event
   * If id > 0 then edit else add event
   */
  async ngOnInit() {
    this.router.paramMap.subscribe(async (params: ParamMap) => {
      this.eventModel = await this.getEventById(Number(params.get('id')));
    });
  }

  /**
   * 
   * Then check is it add or edit operation by event id and send request.
   */
  public async submit() {
    var validation = this.eventModel.validateModel()
    if (validation) return this.showToast(validation);

    this.eventModel.userId = this.user.id;

    if (this.eventModel.id > 0) {
      const addResponse = await this.eventService.editEvent(this.eventModel);
      return addResponse ? this.success() : this.failed();

    } else {
      const editResponse = await this.eventService.addEvent(this.eventModel);
      return editResponse ? this.success() : this.failed();
    }
  }

  /**
   * Gets event by id, If id <= 0 then return EventModel, Else get event from server and return EventModel
   * @param id   
   */
  public async getEventById(id: number) {
    if (id <= 0) return new EventModel();

    var data = await this.eventService.getEvent(id);
    if (data == null || data.result == null) return new EventModel();

    data.result.date = data.result.date.split('T')[0];
    return new EventModel(data.result);
  }

  /**
 * Notify home component using event emitter because, New event added or editted
 */
  public success() {
    this.eventService.eventServiceEmitter.emit(true);
    this.showToast(this.eventModel.id > 0 ? "Event updateded successfully" : "Event added successfully");
    this.eventModel = new EventModel();
  }

  public failed() {
    this.showToast(this.eventModel.id > 0 ? "Event update failed" : "Event add failed");
  }

  /**
   * Common snackbar
   * @param message 
   */
  showToast(message: any) {
    this.snackBar.open(message, null, { duration: 2000, });
  }
}
