import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomePageRoutingModule } from './home-page-routing.module';
import { HomePageComponent } from './home-page.component';
import { AddUpdateEventComponent } from './add-update-event/add-update-event.component';
import { MatCardModule, MatInputModule, MatButtonModule, MatSelectModule, MatDialog } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { ShowEventComponent } from './show-event/show-event.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ModalDialogComponent } from './modal-dialog/modal-dialog.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { SnackBarComponent } from './snack-bar/snack-bar.component';

@NgModule({
    declarations: [HomePageComponent, AddUpdateEventComponent, ShowEventComponent, ModalDialogComponent, SnackBarComponent],
    imports: [CommonModule,
        HomePageRoutingModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule,
        FlexLayoutModule,
        FormsModule,
        MatSelectModule,
        MatDialogModule,
        MatSnackBarModule],
    entryComponents: [ModalDialogComponent, SnackBarComponent]
})
export class HomePageModule { }
