import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CrudService } from 'src/app/core/services/http/crud.service';

@Injectable({
  providedIn: 'root'
})

export class EventService extends CrudService {

  public eventServiceEmitter = new EventEmitter();
  apiUrl = environment.apiEndpoint;
  endpoint: string = 'Event';

  constructor(http: HttpClient) {
    super(http);
  }

  public async addEvent(model: any) {
    try {
      this.url = this.apiUrl;
      this.endpoint = 'Event/Add';
      let response = await this.post(model);
      return response;
    } catch (error) {
      return false;
    }
  }

  public async editEvent(model: any) {
    try {
      this.url = this.apiUrl;
      this.endpoint = 'Event/Edit';
      let response = await this.put(model);
      return response;
    } catch (error) {
      return false;
    }
  }

  public async getEvents(id: number, skip: number, count: number) {
    try {
      this.url = this.apiUrl;
      this.endpoint = 'Event/GetList/' + id + '/' + skip + '/' + count;
      let response = await this.getList();
      return response;
    } catch (error) {
      return false;
    }
  }

  public async getEvent(id: number) {
    try {
      this.url = this.apiUrl;
      this.endpoint = 'Event/Get/' + id;
      let response = await this.getById();
      return response;
    } catch (error) {
      return { result: [], status: 400 };
    }
  }

  public async deleteEvent(id: number) {
    try {
      this.url = this.apiUrl;
      this.endpoint = 'Event/' + id;
      debugger;
      let response = await this.deleteById();
      return response;
    } catch (error) {
      return { result: [], status: 400 };
    }
  }
}