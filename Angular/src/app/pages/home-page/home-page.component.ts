import { Component, OnInit } from '@angular/core';
import { EventService } from './service/event.service';
import { StorageService } from 'src/app/core/services/storage/storage.service';
import { StorageKey } from 'src/app/core/services/storage/storage.model';
import { DateTimeUtil } from '../utility/date-time-util';
import { MatDialog } from '@angular/material/dialog';
import { ModalDialogComponent } from './modal-dialog/modal-dialog.component';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { MatSnackBar } from '@angular/material';
const { USER_DATA } = StorageKey;

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.scss'],
})

export class HomePageComponent implements OnInit {

    user: any;
    eventList: any = [];
    eventModifiedList: any = [];

    constructor(private eventService: EventService, private storage: StorageService, public dialog: MatDialog, private router: ActivatedRoute, private snackBar: MatSnackBar) {
        this.user = JSON.parse(this.storage.read(USER_DATA));
    }

    ngOnInit() {
        this.eventEmiter();
        this.fetchEventList();
    }

    /**
     * After Editing or Deleting, fetch the event list again
     */
    public eventEmiter() {
        this.eventService.eventServiceEmitter.subscribe((resp: any) => {
            this.fetchEventList();
        });
    }

    /**
     * Clear existing event list from home page
     * Fetchs event list for logined in user, modify the data.
     */
    public async fetchEventList() {
        this.eventModifiedList = [];
        let data: any = await this.eventService.getEvents(this.user.id, 0, 20);
        if (data == null || data.result == null || !data.result.length) return this.showToast("No event found");

        this.eventList = data.result;
        this.modifyEventList();
    }

    /**
     * Modify list such a way that all event should grouped by event date, also set each event label color.
     */
    public modifyEventList() {
        let dateList = [];

        this.eventList.forEach((event: any) => {
            let someEvent: any = event;

            if (dateList.findIndex(x => x == someEvent.date) == -1) {
                dateList.push(someEvent.date);
                this.eventModifiedList.push({ isDate: true, data: someEvent.date });
            }

            someEvent.label = '6px solid ' + someEvent.label;
            this.eventModifiedList.push({ isDate: false, data: someEvent });
        });
    }

    /**
     * Convert date (4/6/2020) into user convenient formated date ('Sat Jun 4')
     * @param dateTime 
     * @returns Example 'Sat Jun, 4'
     */
    public getReadableDate(dateTime: any) {
        return DateTimeUtil.getReadableDate(dateTime);
    }

    /**
     * Convert time (14: 00) into user convenient formated time (2 PM)
     * @param timeFrom
     * @param timeTo
     * @returns Example '2 PM'
     */
    public getReadableTime(timeFrom: any, timeTo: any) {
        return DateTimeUtil.getReadableTime(timeFrom, timeTo);
    }

    /**
     * Determines whether edit mode enable nor not
     * @param dateTime 
     * @param timeTo 
     * @returns  
     */
    public isEditModeEnable(dateTime: any, timeTo: any) {
        return DateTimeUtil.isEditVisible(dateTime, timeTo);
    }

    /**
     * Opens delete dialog for confirmation
     * @param id 
     */
    openDeleteDialog(id: number) {
        const dialogRef = this.dialog.open(ModalDialogComponent, { data: { id: id } });
        dialogRef.afterClosed().subscribe(result => {
            debugger;
            if (result == true) this.fetchEventList();
        });
    }

    /**
   * Common snackbar
   * @param message 
   */
    showToast(message: any) {
        this.snackBar.open(message, null, { duration: 2000, });
    }
}