import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';
import { LoginModel } from 'src/app/model/login-model';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss'],
})

export class LoginPageComponent implements OnInit {
    loginModel = new LoginModel();

    constructor(private authService: AuthService, private router: Router, private snackBar: MatSnackBar) { }

    ngOnInit() {
        if (this.authService.isLogged()) {
            this.navigateTo();
        }
    }

    /**
     * Validate loginModel, if ok then send request to login
     * @returns  
     */
    public async login() {
        var validation = this.loginModel.validateModel();
        if (validation) {
            return this.snackBar.open(validation, null, { duration: 2000, });
        }

        try {
            const url = await this.authService.login(this.loginModel);
            this.navigateTo(url);
        } catch (e) {
            return this.snackBar.open("Login failed failed", null, { duration: 2000, });
        }
    }

    /**
     * Redirect to Signups page
     */
    public signup() {
        this.router.navigate(['auth/signup'], { replaceUrl: true });
    }

    public navigateTo(url?: string) {
        url = url || 'nav';
        this.router.navigate([url], { replaceUrl: true });
    }
}
