import { Component, OnInit } from '@angular/core';
import { ImageService } from 'src/app/core/services/http/image.service';
import { AuthService } from 'src/app/auth/auth.service';
import { Router } from '@angular/router';
import { SignupModel } from 'src/app/model/signup-model';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})

export class SignUpComponent implements OnInit {

  localImageFile: any;
  signUpModel = new SignupModel();

  constructor(private imageService: ImageService, private authService: AuthService, private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
    if (this.authService.isLogged()) {
      this.navigateTo();
    }
  }

  /**
   * 1st validate SignUpModel, if its ok then send registration request.
   * If registration request failed then show "Registration failed" toast.
   * @returns  
   */
  public async signup() {
    var validation = this.signUpModel.validateModel();
    if (validation) {
      return this.snackBar.open(validation, null, { duration: 2000, });
    }

    const resp = await this.authService.registration(this.signUpModel);
    if (!resp) return this.snackBar.open("Registration failed", null, { duration: 2000, });

    this.navigateTo(resp);
  }

  /**
   * Go to login page
   */
  public login() {
    this.router.navigate(['auth'], { replaceUrl: true });
  }

  /**
   * Uploads image file to the server, server return image location.
   * Then save it to signUpModel
   * @param imageInput: SignupModel 
   */
  public async uploadImage(imageInput: any) {
    const file: File = imageInput.files[0];
    this.signUpModel.image = await this.imageService.uploadImage(file);

    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (_event) => {
      this.localImageFile = reader.result;
    }
  }

  public navigateTo(url?: string) {
    url = url || 'nav';
    this.router.navigate([url], { replaceUrl: true });
  }
}
