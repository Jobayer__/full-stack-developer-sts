export class DateTimeUtil {
    static months: any = [
        'JANUARY',
        'FEBRUARY',
        'MARCH',
        'APRIL',
        'MAY',
        'JUNE',
        'JULY',
        'AUGUST',
        'SEPTEMBER',
        'OCTOBER',
        'NOVEMBER',
        'DECEMBER'
    ];

    static days: any = [
        'SUNDAY',
        'MONDAY',
        'TUESDAY',
        'WEDNESDAY',
        'THURSDAY',
        'FRIDAY',
        'SATURDAY'
    ]

    static getReadableDate(dateTime: any) {
        const date = new Date(dateTime);

        const monthIndex = date.getMonth();
        const monthName = DateTimeUtil.months[monthIndex];

        const dayIndex = date.getDay()
        const dayName = DateTimeUtil.days[dayIndex];

        return dayName + ', ' + monthName + ' ' + date.getDate();
    }

    static getReadableTime(timeFrom: any, timeTo: any) {
        let tf = Number(timeFrom.split(":")[0]);
        let tt = Number(timeTo.split(":")[0]);

        if (tf == tt) return (tf > 12 ? tf - 12 : tf) + (tf > 12 ? 'pm' : 'am');
        if (tf > 12) tf = tf - 12;
        return tf + ' - ' + (tt > 12 ? tt - 12 : tt) + (tt > 12 ? 'pm' : 'am');
    }

    static isEditVisible(dateTime: any, timeTo: any) {
        const eventTime = new Date(dateTime).getTime()
        const currentTime = new Date().getTime();

        const eventHour = (Number(timeTo.split(':')[0]) * 60 * 60 * 1000) + (Number(timeTo.split(':')[1] * 60 * 1000));

        if (eventTime + eventHour >= currentTime) return true;
        return false;
    }

    static isValidDate(date: Date) {
        return !isNaN(new Date(date).getTime());
    }

    static isValidTime(time: any) {
        if (!time || time.length < 1) { return false; }
        var time = time.split(':');

        return time.length >= 2
            && parseInt(time[0], 10) >= 0
            && parseInt(time[0], 10) <= 23
            && parseInt(time[1], 10) >= 0
            && parseInt(time[1], 10) <= 59;
    }

    static isTimeFromLessThanTimeTo(timeFrom: any, timeTo: any) {
        const timeFromMinute = Number(timeFrom.split(':')[0]) * 60 + Number(timeFrom.split(':')[1]) + (timeFrom.split(':')[2] == 'PM' ? 12 * 60 : 0);
        const timeToMinute = Number(timeTo.split(':')[0]) * 60 + Number(timeTo.split(':')[1]) + (timeTo.split(':')[2] == 'PM' ? 12 * 60 : 0);
        
        return !(timeFromMinute <= timeToMinute);
    }
}