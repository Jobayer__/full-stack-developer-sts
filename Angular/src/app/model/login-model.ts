export class LoginModel {

    email: string;
    password: string;

    constructor() {
    }

    public validateModel() {
        if (this.email == null || this.email.length == 0) {
            return "Please provide a valid email";
        } else if (this.password == null || this.password.length < 5) {
            return "Invalid password";
        }
    }
}