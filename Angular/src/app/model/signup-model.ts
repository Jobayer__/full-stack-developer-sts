export class SignupModel {
    id: number;
    fullName: string;
    email: string;
    password: string;
    image: string;
    birthDate: Date;

    constructor() { }

    /**
     * Validates SignupModel
     * @returns  
     */
    public validateModel() {
        if (this.fullName == null || this.fullName.length == 0) {
            return "Please provide your name";
        } else if (this.email == null || this.email.length == 0) {
            return "Please provide a valid email";
        } else if (this.password == null || this.password.length < 5) {
            return "Password length should be greater than 4";
        } else if (this.image == null || this.image.length == 0) {
            return "Please upload an image";
        }
        return null;
    }
}