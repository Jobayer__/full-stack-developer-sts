import { Time } from '@angular/common';
import { DateTimeUtil } from '../pages/utility/date-time-util';

export class EventModel {
    id: number;
    userId: number;
    title: string;
    description: string;
    date: Date;
    timeFrom: Time;
    timeTo: Time;
    location: string;
    notifyTime: number;
    label: string;

    /**
     * Creates an instance of event model.
     * @param [model] 
     */
    constructor(model: any = {}) {
        this.id = model.id;
        this.userId = model.userId;
        this.title = model.title;
        this.description = model.description;
        this.date = model.date;
        this.timeFrom = model.timeFrom;
        this.timeTo = model.timeTo;
        this.location = model.location;
        this.notifyTime = model.notifyTime;
        this.label = model.label;
    }

    /**
     * Validate model before add/update Event
     * @returns  
     */
    public validateModel() {
        if (this.title == null || this.title.length == 0) {
            return "Please provide a Title";
        } else if (this.description == null || this.description.length == 0) {
            return "Please provide a Description";
        } else if (!DateTimeUtil.isValidDate(this.date)) {
            return "Please provide a Date";
        } else if (!DateTimeUtil.isValidTime(this.timeFrom)) {
            return "Please provide Time From";
        } else if (!DateTimeUtil.isValidTime(this.timeTo)) {
            return "Please provide Time To";
        } else if (this.location == null || this.location.length == 0) {
            return "Please provide Location";
        } else if (this.notifyTime == null || this.notifyTime <= 0) {
            return "Please provide Notify Time";
        } else if (this.label == null || this.label.length == 0) {
            return "Please provide Label";
        }
        return null;
    }
}