import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { CrudService } from './crud.service';

@Injectable({
    providedIn: 'root',
})

export class ImageService extends CrudService {
    apiUrl = environment.apiEndpoint;
    endpoint: string = 'ImageUpload';

    constructor(http: HttpClient) {
        super(http);
    }
  
    public async uploadImage(image: File) {
        try {
            this.url = this.apiUrl;

            let imageName = await this.upload(image);
            return imageName;
        } catch (error) {
            console.error('Error during login request', error);
            return Promise.reject(error);
        }
    }
  }