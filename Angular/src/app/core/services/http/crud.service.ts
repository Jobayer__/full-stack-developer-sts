import { HttpClient, HttpErrorResponse } from '@angular/common/http';

export abstract class CrudService<T = any> {
    abstract endpoint: string;
    url: string = ('');

    protected constructor(protected http: HttpClient) { }

    public async get<G>(): Promise<G | null> {
        let response = null;
        try {
            response = await this.http
                .get<G>(`${this.url}/${this.endpoint}`)
                .toPromise();
        } catch (error) {
            response = this.errorHandler('GET', error);
        }
        return response;
    }

    public async getList(): Promise<T[] | null> {
        return this.get<T[]>();
    }

    public async getById(): Promise<T | null> {
        return this.get<T>();
    }

    public async post(body): Promise<any> {
        let response = null;
        try {
            response = await this.http
                .post(`${this.url}/${this.endpoint}`, body)
                .toPromise();
        } catch (error) {
            response = this.errorHandler('POST', error);
        }
        return response;
    }

    public async put(body): Promise<any> {
        let response = null;
        try {
            response = await this.http
                .put(`${this.url}/${this.endpoint}`, body)
                .toPromise();
        } catch (error) {
            response = this.errorHandler('POST', error);
        }
        return response;
    }

    public async deleteById(): Promise<any> {
        let response = null;
        try {
            response = await this.http
                .delete(`${this.url}/${this.endpoint}`)
                .toPromise();
        } catch (error) {
            response = this.errorHandler('DELETE', error);
        }
        return response;
    }

    public async upload(image: File): Promise<any> {
        const formData = new FormData();
        formData.append('image', image);

        let response = null;
        try {
            response = await this.http
                .post(`${this.url}/${this.endpoint}`, formData, { responseType: 'text' })
                .toPromise();
        } catch (error) {
            response = this.errorHandler('POST', error);
        }
        return response;
    }

    public errorHandler(
        method: string,
        error: HttpErrorResponse,
    ): Promise<never> {
        console.error(
            `Error occurred during ${method} ${this.url}/${this.endpoint}`,
            error,
        );
        return Promise.reject(error);
    }
}
