import { Component, OnInit } from '@angular/core';
import {
    NavigationService,
    Page,
} from '../../services/navigation/navigation.service';
import { NavRoute } from '../../../nav-routing';
import { AuthService } from '../../../auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageService } from '../../services/storage/storage.service';
import { StorageKey } from '../../services/storage/storage.model';
import { environment } from 'src/environments/environment';
const { USER_DATA } = StorageKey;

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
    isOpen = true;
    profileImage: string;

    constructor(
        private navigationService: NavigationService,
        private authService: AuthService,
        private router: Router,
        private storage: StorageService
    ) {}

    ngOnInit() {
        const model = JSON.parse(this.storage.read(USER_DATA));
        this.profileImage = environment.baseEndpoint + '/' + model.image;
    }

    public toggleSideNav() {
        this.isOpen = !this.isOpen;
    }

    public getNavigationItems(): NavRoute[] {
        return this.navigationService.getNavigationItems();
    }

    public getActivePage(): Page {
        return this.navigationService.getActivePage();
    }

    public logout() {
        this.authService.logout();
        this.router.navigate(['login'], { replaceUrl: true });
    }

    public getPreviousUrl(): string[] {
        return this.navigationService.getPreviousUrl();
    }

    public onClickHome(){
        debugger;
        this.router.navigate(['nav/home']);
    }

    public onClickNewEvent(){
        this.router.navigate(['nav/home/add-edit-event/0']);
    }

    public onClickLogout(){
        this.authService.logout();
        this.router.navigate(['login'], { replaceUrl: true });
    }
}
