import { Injectable } from '@angular/core';
import { CrudService } from '../core/services/http/crud.service';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../core/services/storage/storage.service';
import { StorageKey } from '../core/services/storage/storage.model';
import { environment } from 'src/environments/environment';
const { AUTH_TOKEN, USER_DATA } = StorageKey;

@Injectable({
    providedIn: 'root',
})
@Injectable({
    providedIn: 'root',
})

export class AuthService extends CrudService {
    apiUrl = environment.apiEndpoint;
    token: string;
    redirectUrl: string = "/";
    endpoint: string;

    constructor(http: HttpClient, private storage: StorageService) {
        super(http);
        this.token = this.storage.read(AUTH_TOKEN) || '';
    }

    public async login(loginModel: any) {
        try {
            this.url = this.apiUrl;
            this.endpoint = 'Authentication/Login';

            let data = await this.post(loginModel);
            if (data.status && data.token) {
                this.token = data.token;
            } else {
                throw console.error();
            }

            this.storage.save(AUTH_TOKEN, this.token);
            this.storage.save(USER_DATA, JSON.stringify(data));
            return this.redirectUrl;
        } catch (error) {
            console.error('Error during login request', error);
            return Promise.reject(error);
        }
    }

    public async registration(model: any) {
        try {
            this.url = this.apiUrl;
            this.endpoint = 'Authentication/Signup';

            let data = await this.post(model);
            if (data.result.id > 0) {
                return await this.login({ email: model.email, password: model.password });
            }
        } catch (error) {
            return false;
        }
    }

    public getToken(): string {
        return this.token;
    }

    public logout() {
        this.token = '';
        this.storage.remove(AUTH_TOKEN);
    }

    public isLogged(): boolean {
        return this.token.length > 0;
    }
}
